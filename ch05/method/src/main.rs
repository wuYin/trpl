#[derive(Debug)]
struct Rectangle {
    w: u32,
    h: u32,
}

impl Rectangle {
    // 第一个参数有 3 种情况
    // 1. &self 不借用所有权，仅读
    // 2. &mut self 不借用所有权，读写
    // 3. self 获取所有权
    fn area(&self) -> u32 {
        self.h * self.w
    }

    fn can_hold(&self, r2: &Rectangle) -> bool {
        self.w > r2.w && self.h > r2.h
    }
}

// 可有多个 Rectangle 块
impl Rectangle {
    // 关联函数常用作构造函数，并非方法，如 String::from("__")
    fn square(wh: u32) -> Rectangle {
        Rectangle { w: wh, h: wh }
    }
}

fn main() {
    let r1 = Rectangle { w: 10, h: 2 };
    println!("area: {}", r1.area());

    let r2 = Rectangle { w: 1, h: 1 };
    println!("can_hold: {}", r1.can_hold(&r2));

    let p1 = Point { x: 0.0, y: 0.0 };
    let p2 = Point { x: 3.0, y: 4.0 };

    // distance 的第一个参数让 Rust 能对应解引用
    println!("distance: {}", p1.distance(&p2));
    println!("distance: {}", (&p1).distance(&p2));


    let sq = Rectangle::square(3);
    println!("sq: {:#?}", sq);
}


// feature: Rust 自动解引用
#[derive(Debug, Copy, Clone)]
struct Point {
    x: f64,
    y: f64,
}

impl Point {
    fn distance(&self, p2: &Point) -> f64 {
        let x = f64::powi(p2.x - self.x, 2);
        let y = f64::powi(p2.y - self.y, 2);
        f64::sqrt(x + y)
    }
}

