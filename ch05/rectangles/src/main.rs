fn main() {
    let w = 10;
    let h = 2;
    println!("area1: {}", area1(w, h));

    let vs = (10, 2);
    println!("area2: {}", area2(vs));

    let r = Rectangle { w: 10, h: 2 };
    println!("area3: {}", area3(&r));

    println!("r: {:?}", r);
    println!("r: {:#?}", r);
}

fn area1(w: u32, h: u32) -> u32 {
    w * h
}

fn area2(vs: (u32, u32)) -> u32 {
    vs.0 * vs.1 // 元组索引不能指定 0 为 w，1 为 h
}


// 添加注解来派生 Debug trait
#[derive(Debug)]
struct Rectangle {
    w: u32,
    h: u32,
}

// 引用参数不取 ownership
fn area3(r: &Rectangle) -> u32 {
    r.w * r.h
}