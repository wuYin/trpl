fn main() {
    let mut u1 = build_user(String::from("wuYin"), String::from("wuYin@gmail.com"));
    u1.active = false;

    let u2 = User {
        active: true,
        ..u1 // f2：更新实例创建实例
    };

    // f3: 元组结构体
    struct Point(i32, i32, i32);
    let mut ori = Point(0, 0, 0);
    ori.1 = 100;
    let Point(x, y, z) = ori; // 不能解构为 tuple，需解构为同一类型
    println!("origin: {}, y: {}", ori.1, y);
}

struct Person {
//    username: &str, // 引用类型需设置声明周期 lifetime
    age: ui32,
}

// Rust 不允许 struct 部分字段 mut，相当于整体 public / private
struct User {
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool,
}

fn build_user(uname: String, email: String) -> User {
    User {
        email, // f1：形参字段同名则简化显式指定字段名，顺序随意
        username: uname,
        active: true,
        sign_in_count: 10,
    }
}
