fn main() {
    println!("Hello, world!");
    f1();
    f2(1, 2);

// 语句 statements，表达式 expressions
//    let x = (let y = 19); // let 是语句无返回值，但表达式返回值。有别于 C 的赋值表达式返回所赋的值
//    let x = y = 0; // 同理 y = 0 是赋值语句，不是赋值表达式
// 函数、宏调用、{} 都是表达式
    let y = {
        let x = 1;
        x + 1 // 计算式不加分号作为值是表达式，加分号为语句
    };
    println!("y: {}", y); // 2

    println!("five(): {}", five());
    println!("five_plus(5): {}", five_plus(5));
}


fn f1() {
    println!("function f1 defined after main, cool");
}

// 形参 parameters，实参 arguments
// 类型声明
// [Rust] var: type
// [C]    type var
// [Go]    var type
fn f2(x: i32, y: i64) {
    println!("x, y: {}, {}", x, y);
}

fn five() -> i32 {
    5
}

// snake_case 风格
fn five_plus(x: i32) -> i32 {
//    return x + 1;
    x + 1
}