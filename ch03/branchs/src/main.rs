fn main() {
    let n = 3;
    if n < 5 {
        println!("ok");
    } else {
        println!("not ok");
    }

//    let b = 1;
//    if b { println!("ok"); } // 同样不能像 C 一样混用 int 和 bool


    let n = 6;
    if n % 4 == 0 {
        println!("{} divisible by 4", n);
    } else if n % 3 == 0 {
        println!("{} divisible by 3", n);
    } else if n % 2 == 0 {
        println!("{} divisible by 2", n);
    } else {
        println!("{} divisible nothing", n);
    }

    let ok = true;
    let res = if ok { 5 } else { 6 }; // 数字本身就是表达式
    println!("res: {}", res);
}
