fn main() {
    // 多种类型可能时，需手动 annotation
    // let var: type = ...
    let guess: i32 = " 10\n".trim().parse().expect("must be number");
    println!("guess: {}", guess);


    // 标量类型
    let x: u16 = 20_100; // 整型内部可随意 _ 分割
    let x: i128 = 1000_000; // 128 16 字节类型对应 long long
    println!("x: {}", x);
//    let y = 2_56u8; // 类型溢出，编译错误 out of range

    let x = 1.1; // 默认双精度 f64
    let y: f32 = 1.2;
    let b: bool = false;
    let _c: char = '🌚'; // Rust 的字符是 Unicode 标量值，范围远比 ASCII 广泛


    // 复合类型，定义后长度不变
    // 元组 tuple // 将其他多个类型组合的复合类型，类似于 C 中只有成员变量的 struct
    let tup: (i32, f64, u8) = (10, 1.1, 2);
    let tup = (1, 2.1, 0b0001_0001); // 类型注解可选
    let (x, y, z) = tup; // 使用模式匹配将 tuple 解构
    println!("tup.0: {}", tup.0); // 索引访问

    // 数组
    // 类型一致，长度固定
    let vs = [2, 10];// 2 10 // , 列表初始化
    let vs: [i32; 4] = [1, 2, 3, 4]; // 指定类型和长度
    let vs = [2; 10];// 2 2 2 // ; 批量初始化
//    let v = vs[10]; // 越界 panic
}
