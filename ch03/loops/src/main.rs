fn main() {
    // loop 无限循环直到手动停止
    let mut c = 0;
    let res = loop {
        c += 1;
        if c == 10 {
            break c * 2; // break 关键字能像 return 一样提前返回，合理地也能返回值，其为表达式
        }
    };
    println!("res: {}", res);

    // while 有条件循环
    let mut c = 3;
    loop {
        if c != 0 {
            println!("{}!", c);
        } else {
            break;
        };
        c -= 1;
    }

    let mut c = 3;
    while c != 0 {
        println!("{}!", c);
        c -= 1;
    }

    // for 遍历循环
    let arr = [10, 20, 30, 40, 50];
    let mut idx = 0;
    while idx < 5 {
        println!("v: {}", arr[idx]);
        idx += 1;
    }
    // Rust 的 range 循环
    for v in arr.iter() {
        println!("vv: {}", v);
    }
    // 生成序列，类似 Shell 的 {1..3}
    for v in (1..4).rev() {
        println!("vvv: {}", v);
    }
}
