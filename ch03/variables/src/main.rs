fn main() {
    // mut 手动标记 x 为可变变量
    let mut x = 1;
    println!("x: {}", x);
    x = 2;
    println!("x: {}", x);

    // 显式类型声明常量
    const MAX_POINTS: u32 = 1000;

    // 重定义 shadow 同名变量
    let x = 5;
    let x = x + 1;
    let x = x * 2;
    println!("x: {}", x);

    // shadowing 区别于 mut，其定义了新的变量，可修改数据类型
    let spaces = "   ";
    let spaces = spaces.len();
    println!("spaces: {}", spaces);
}