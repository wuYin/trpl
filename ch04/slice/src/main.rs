fn main() {
    string_slice();


    let s = String::from("abc DEF");
    println!("idx: {}", first_word(&s));
    let w = first_word_2(&s);
    println!("word: {}", w);
    // s.clear(); // 此处发生了 mut reference borrowed，与之前的 &s 只读存在 data race，编译错误

    let s = String::from("abc DEF");
    let w = first_word_3(&s[..]);
    println!("w: {}", w);
    let s = "abc DEF";
    let w = first_word_3(&s[..]);
    println!("w: {}", w);
}

// &str 字符串字面值更通用，String 可传递整个 slice
fn first_word_3(s: &str) -> &str {
    let bytes = s.as_bytes();
    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[..i];
        }
    }
    &s[..i
}


fn first_word_2(s: &String) -> &str {
    let bytes = s.as_bytes();
    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[..i];
        }
    }
    &s[..]
}

fn string_slice() {
    let s = String::from("_abcDEF");
    let s1 = &s[1..3];
    let s2 = &s[..];
    let s3 = &s1[1..2]; // &str 可切出 &str
    println!("s1: {}, s2: {}, s3: {}", s1, s2, s3);
}

fn first_word(s: &String) -> usize {
    let bytes = s.as_bytes();
    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return i;
        }
    }
    s.len()
}