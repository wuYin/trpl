// 栈：数据在编译时占用已知且固定的大小空间。无需搜索可用内存地址直接栈顶入栈，分配更快。连续出栈访问速度也更快
// 堆：向 OS 请求分配块内存

// 所有权系统解决的三个问题
// 1. 追踪代码对应堆上的数据
// 2. 减少堆上重复的数据
// 3. 清理堆上不再使用的数据

// 所有权系统的两个原则
// 1. 每个值都有且仅有一个 owner 变量
// 2. 当 owner 变量离开作用域时，值被丢弃

// 变量在离开作用域后内存释放

fn main() {
    let mut s1 = String::from("hello"); // 向 OS 请求堆内存
    s1.push_str(", world");
    println!("s1 :{}", s1);


    // 变量与数据交互方式
    // 1. value move
    let s2 = s1;
    // println!("s1 :{}", s1);


    // 2. clone
    let s3 = s2.clone();
    println!("s2 :{}, s3 :{}", s2, s3);

    // 3. stack data copy
    let x = 1;
    let y = x;
    println!("x :{}, y :{}", x, y);


    // 所有权与函数
    let s = String::from("abc");
    take_ownership(s);
    // println!("s :{}", s);

    let x = 1;
    make_copy(x);
    println!("x :{}", x);


    // 返回值与作用域
    let s1 = give_ownership();
    let s2 = String::from("ABC");
    let s3 = take_and_give_back(s2);
} // drop(s) // Drop trait 与 Copy Trait 互斥


fn give_ownership() -> String {
    let tmp = String::from("abc");
    tmp
}

fn take_and_give_back(s: String) -> String {
    s
}


fn take_ownership(s: String) {
    println!("s :{}", s);
}

fn make_copy(i: i32) {
    println!("i :{}", i);
}