fn main() {
    // 函数调用只需值，不需 ownership
    rid_ownership();

    // 引用取值，但不拥有
    borrow_ownership();

    // 可变引用
    mutable_reference();

    ref_skip_scope_ok();
}

// 悬挂指针
// Rust 保证引用总是有效的
/*
fn dangle() -> &String {
   let s = String::from("abc");
   &s
}
*/


// Rust 在编译时在语法层面保证无 data race
fn ref_skip_scope_ok() {
    let mut s = String::from("abc");
    {
        let r1 = &s;
    }
    let r2 = &mut s; // ok
}

/*
fn reference_no_mut_or_mut() {
    let s = String::from("abc");
    let r1 = &s;
    let r2 = &s;
    println!("r1: {}, r2: {}", r1, r2);

    let r3 = &mut s; // r3 可能修改 s 不被允许
}


fn one_scope_one_mut_reference() {
    let mut s1 = String::from("abc");
    let m1 = &mut s1;
    let m2 = &mut s1;
    println!("m1: {}, m2: {}", m1, m2); // m1 和 m2 存在并发写不安全问题
}
*/

fn mutable_reference() {
    let mut s = String::from("abc");
    change(&mut s);
    println!("s: {}", s);
}

fn change(s: &mut String) {
    s.push_str("__");
}

fn borrow_ownership() {
    let s = String::from("abc");
    let len = get_len2(&s);
    println!("s: {}, len: {}", s, len);
}

fn get_len2(s: &String) -> usize {
    let n = s.len();
    n
}

fn rid_ownership() {
    let s1 = String::from("abc");
    let (s2, len) = get_len(s1);
    println!("s2: {}, len: {}", s2, len);
}

fn get_len(s: String) -> (String, usize) {
    let n = s.len();
    (s, n)
}
