// 手动导入标准库
use std::io;
// Rng 是 trait 类似接口，定义随机数生成器必须实现的方法
use rand::Rng;
use std::cmp::Ordering;

fn main() {
    println!("Guess a number");
    // 在当前线程中获取操作系统的 seed，gen_range 为 Rng trait 定义的方法
    let secret_number = rand::thread_rng().gen_range(1, 101);
    //    println!("Secret number: {}", secret_number);

    loop {
        println!("Please input a number: ");

        // 显式创建可变变量，并用 String 的静态方法创建 UTF8 空字符串初始化
        let mut guess = String::new();

        // read_line 只读结果始终为字符串
        // 和 C++ 类似 Rust 中也有引用避免拷贝值开销，需带上修饰符 mut，有点像指针参数
        // read_line 返回 io::Result 枚举 Ok 和 Err 成员，对应做错误处理
        // Result 本身是泛型，io 和 cmp 都实现
        io::stdin().read_line(&mut guess)
            .expect("Failed to read line");

        // 隐藏同名变量，类型转换常用
        // var: type 显式指定变量类型
        // expect 转为 match，即从崩溃到错误处理；类似 Go 加不加 recover()
        let guess: u32 = match guess.trim().parse() { // 需手动 trim 掉 read_line 读入的右侧 \n
            Ok(num) => num, // Ok 表示匹配成功，内部包含解析后的值
            Err(_) => continue, // 代码块即可
        };

        println!("You inputted: {}", guess);

        // cmp 可在任何可比较值上调用，返回 Ordering 枚举类型的三个成员之一
        // match 表达式，类似于 switch 多路判断，表达式的值是匹配后代码执行结果
        // arms 分支：pattern 模式 => 对应执行的代码
        match guess.cmp(&secret_number) {
            Ordering::Less => println!("too small"),
            Ordering::Greater => println!("too big"),
            Ordering::Equal => {
                println!("bingo");
                break;
            }
        };
    }
}
